<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>داروخانه</title>
        <link rel="stylesheet" href="asset/drugstore.css"/>
        <script src="asset/js/jquery.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular-route.js"></script>
    </head>
    <body ng-app="mphj">
        <div class="nav">
            <span class="title">داروخانه</span>
        </div>
        <div class="right-menu">
            <ul>
                <li class="active" onclick="makeActive(this);"><a href="#/!">صفحه ی داروخانه</a><li>
                <li onclick="makeActive(this);"><a href="#!first">صحفحه ی پزشک عمومی</a><li>
                <li onclick="makeActive(this);"><a href="#!second">صفحه ی سابقه ی بیمار</a><li>
            </ul>
        </div>
        <div class="body">
            <ng-view></ng-view>
        </div>
    </body>
    <script>
        var app = angular.module("mphj", ["ngRoute"]);
        app.config(function ($routeProvider) {
            $routeProvider
                    .when("/", {
                        templateUrl: "drugstore_tajviz.html"
                    })
                    .when("/first", {
                        templateUrl: "doctor_history.html"
                    })
                    .when("/second", {
                        templateUrl: "sick_history.html"
                    })
                    .when("/blue", {
                        templateUrl: "blue.htm"
                    });
        });
        
        function makeActive(ele){
            ele = $(ele);
            $(".right-menu li").removeClass("active");
            ele.addClass("active");
        }
    </script>
</html>
